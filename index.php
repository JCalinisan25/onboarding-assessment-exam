<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>IH ASSESSMENT EXAM</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <!-- Task 1 - Create an html page with the ff elements: header title, paragraph text, 
     1 text link inside paragraph, button that shows an alert box (javascript)-->
    <table>
        <tr>
            <td> 
                <h1>Onboarding Assessment Exam</h1>
            </td>
        </tr>

        <tr>
            <td>
                <p>A mysterious <a href="https://docs.google.com/document/d/1IxK79gybLhoIOIWP9rTaUg3XiRyMMXmgqntizU82WpA/edit" target="_blank">link</a> binds the fates of two characters,<br>
                guiding them towards an unforeseen adventure. <br> As the story unfolds, 
                the invisible link between them <br> grows stronger, revealing the profound 
                connection that <br> shapes their destinies.
                </p>
            </td>
            
            <td>
                <script>
                    function showAlert() {
                        alert("Hello, Hi, Kamusta!");
                    }
                </script>
                <div class="but1">
                <button class = "styled-button", onclick="showAlert()"><b>Click me</b></button>
                </div>
            </td>
        </tr>
        
        <tr>
            <td>
                <ul>
                    <?php
                    // Task 2 - Given the ff array of values, output it as an unordered list.
                        $array = ["the", "quick", "brown", "fox"];
                        foreach ($array as $array) {
                            echo "<li>$array</li>";
                        }
                    ?>
                </ul>
            </td>
        </tr>

        <tr>
            <td>
                <?php
                // Task 3 - Create a php function that will accept 3 numbers and output the ff values:       
                    function calculateValues($num1, $num2, $num3) {
                        $sum = $num1 + $num2 + $num3;
                        $average = $sum / 3;
                        $minValue = min($num1, $num2, $num3);
                        $maxValue = max($num1, $num2, $num3);

                        echo "<p>PHP Solution: 10,20,30</p>";
                        echo "<p>Sum: $sum</p>";
                        echo "<p>Average: $average</p>";
                        echo "<p>Min Value: $minValue</p>";
                        echo "<p>Max Value: $maxValue</p>";
                    }

                // Calling the function with static values
                    calculateValues(10, 20, 30);
                ?>
            </td>
        </tr>
     </table>
</body>
</html>